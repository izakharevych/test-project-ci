FROM node:12

COPY *.json .
COPY *.js .

EXPOSE 3000

CMD ["npm", "start"]